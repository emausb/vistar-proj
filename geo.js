var jsonfile = require('jsonfile')
var turf = require('@turf/turf');
var { get } = require('lodash');
var cache = require('memory-cache');

function analyzeCoordinates(latitude, longitude) {
	const statesData = cache.get('statesData');
	const turfPoint = turf.point([longitude, latitude]);

  const containingState = statesData.find((state) => {
  	const turfsStatePolygon = turf.polygon([state.border]);
  	return turf.booleanPointInPolygon(turfPoint, turfsStatePolygon);
  });

  return containingState;
}


module.exports = {
	getState: (req, res) => {
		const { latitude, longitude } = get(req, 'body', '');
		if (!latitude || !longitude) {
			return res.status(422).json('Both latitude and longitude required.');
		}

		const containingState = analyzeCoordinates(latitude, longitude);
		const containingStateName = get(containingState, 'state', '');

		return res.json([containingStateName]);
	},
}
