# Brett's State Server

This server loads the states.json file in to memory on app boot. The server wont have to make a file access request or a request to an external data source. State bounds wont change and this was a very small dataset so it seemed like the right solution for this problem.

If we had a large amount more data it would make more sense to store it in a database that could handle geospatial queries. I've used mongodb for this on a previous project when we had 1000s of polygons, some of which occasionally changed bounds and we needed to know where points where.

## To Run

This is a node server. 

Dependencies:
- node
- npm

Within the project directory, run:

```npm install
node index.js```

You should get a prompt telling you the server is listening and ready to receive requests.
