var express = require('express');
var app = express();
var bodyparser = require('body-parser');

var geo = require('./geo');
var { loadDataToCache } = require('./lib');

loadDataToCache();

app.use(bodyparser.urlencoded({ extended: false }));

// route
app.post('/', geo.getState);

app.listen(8080, () => {
	console.log('I am listening...');
});
