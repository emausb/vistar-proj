var jsonfile = require('jsonfile');
var cache = require('memory-cache');
var filePath = './states.json';

module.exports = {
	loadDataToCache: () => {
		jsonfile.readFile(filePath, function(err, states) {
			if (err) {
				console.log(`Error reading the file. ${err}`);
			} else {
				cache.put('statesData', states);
				console.log('states data saved to server cache.');
			}
		});
	}	
}
